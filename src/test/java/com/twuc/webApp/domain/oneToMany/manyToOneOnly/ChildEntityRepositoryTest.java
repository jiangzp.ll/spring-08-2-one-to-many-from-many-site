package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ClosureValue<Long> closureValue = new ClosureValue<>();

        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");

        childEntity.setParentEntity(parentEntity);
        ChildEntity anotherChildEntity = new ChildEntity("anotherChild");
        anotherChildEntity.setParentEntity(parentEntity);

        flushAndClear(em -> {
            childEntityRepository.save(childEntity);
            childEntityRepository.save(anotherChildEntity);
            ParentEntity savedParentEntity = parentEntityRepository.save(parentEntity);
            closureValue.setValue(savedParentEntity.getId());
        });

        List<ChildEntity> children = childEntityRepository.findAll();
        ChildEntity fetchChildEntity = children.get(0);
        assertEquals("parent", fetchChildEntity.getParentEntity().getName());
        assertEquals(2, children.size());
        assertEquals("child", children.get(0).getName());
        assertEquals("anotherChild", children.get(1).getName());
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> closureValue = new ClosureValue<>();

        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");

        childEntity.setParentEntity(parentEntity);

        flushAndClear(em -> {
            ChildEntity savedChildEntity = childEntityRepository.save(childEntity);
            parentEntityRepository.save(parentEntity);
            closureValue.setValue(savedChildEntity.getId());
        });

        flushAndClear(em -> {
            ChildEntity fetchChildEntity = childEntityRepository.findById(closureValue.getValue()).get();
            fetchChildEntity.setParentEntity(null);
        });

        List<ChildEntity> children = childEntityRepository.findAll();
        assertEquals(1, children.size());
        ParentEntity parentEntityFound = children.get(0).getParentEntity();
        assertNull(parentEntityFound);
        assertEquals(1, parentEntityRepository.findAll().size());

        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ClosureValue<Long> closureValue = new ClosureValue<>();

        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");

        childEntity.setParentEntity(parentEntity);

        flushAndClear(em -> {
            ChildEntity savedChildEntity = childEntityRepository.save(childEntity);
            parentEntityRepository.save(parentEntity);
            closureValue.setValue(savedChildEntity.getId());
        });

        flushAndClear(em -> {
            ChildEntity fetchChildEntity = childEntityRepository.findById(closureValue.getValue()).get();
            childEntityRepository.deleteById(closureValue.getValue());
            parentEntityRepository.deleteById(fetchChildEntity.getParentEntity().getId());
        });

        assertEquals(0, childEntityRepository.findAll().size());
        assertEquals(0, parentEntityRepository.findAll().size());
        // --end-->
    }
}
