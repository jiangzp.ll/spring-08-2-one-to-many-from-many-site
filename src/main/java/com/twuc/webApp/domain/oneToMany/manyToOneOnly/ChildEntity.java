package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import javax.persistence.*;
import java.util.List;

// TODO
//
// 请仅仅从 ChildEntity 定义 ChildEntity 和 ParentEntity 的 one-to-many 关系。其中 ChildEntity
// 对应的数据表定义应当满足如下的条件：
//
// child_entity
// +───────────────────+──────────────+──────────────────────────────+
// | Column            | Type         | Additional                   |
// +───────────────────+──────────────+──────────────────────────────+
// | id                | bigint       | primary key, auto_increment  |
// | name              | varchar(20)  | not null                     |
// | parent_entity_id  | bigint       | null                         |
// +───────────────────+──────────────+──────────────────────────────+
//
// <-start-
@Entity
public class ChildEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20 , nullable = false)
    private String name;

    @ManyToOne
    private ParentEntity parentEntity;

    public ChildEntity() {
    }

    public ChildEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ParentEntity getParentEntity() {
        return parentEntity;
    }

    public void setParentEntity(ParentEntity parentEntity) {
        this.parentEntity = parentEntity;
    }
}
// --end->
